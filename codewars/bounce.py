#!/usr/bin/env python


def bouncingBall(h, bounce, window):
    count = 1
    if h < 0 or bounce <= 0 or bounce >= 1 or window >= h:
        return -1
    else:
        h *= bounce
        while h > window:
            count += 2
            h = h * bounce
            print(h)
            print(count)
    return count

bouncingBall(3, 0.66, 1.5)
